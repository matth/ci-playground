FROM archlinux/archlinux:base

# Update and install everything
RUN pacman --noconfirm --needed -Syu
RUN cat /etc/pacman.conf
